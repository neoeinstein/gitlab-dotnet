#!/usr/bin/env bash
set -e  # Exit immediately upon failure
set -o pipefail  # Carry failures over pipes

repo_root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
docker_repo="registry.gitlab.com/neoeinstein/gitlab-dotnet"

function build_dockerfiles {
    for dockerfile_dir in ${1}; do
        tag="${docker_repo}:$( sed -e 's/.\///' -e 's/debian\///' -e 's/\//-/g' <<< "${dockerfile_dir}" )"
        echo "----- Building ${tag} -----"
        docker build --no-cache -t "${tag}" "${dockerfile_dir}"
    done
}

pushd "${repo_root}" > /dev/null

build_dockerfiles "$( find . -path './.*' -prune -o -name 'Dockerfile' -a -print0 | xargs -0 -n1 dirname)"

popd > /dev/null
