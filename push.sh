#!/usr/bin/env bash
set -e  # Exit immediately upon failure
set -o pipefail  # Carry failures over pipes

repo_root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
docker_repo="registry.gitlab.com/neoeinstein/gitlab-dotnet"

function push_dockerimages {
    for dockerfile_dir in ${1}; do
        tag="${docker_repo}:$( sed -e 's/.\///' -e 's/debian\///' -e 's/\//-/g' <<< "${dockerfile_dir}" )"
        echo "----- Pushing ${tag} -----"
        docker push "${tag}"
    done
}

pushd "${repo_root}" > /dev/null

push_dockerimages "$( find . -path './.*' -prune -o -name 'Dockerfile' -a -print0 | xargs -0 -n1 dirname)"

popd > /dev/null
